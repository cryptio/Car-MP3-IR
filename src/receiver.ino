#include "IRremote.h"

const unsigned BAUDRATE = 9600;
const unsigned RECV_PIN = 11;
const char EOF_CHAR = '!';
IRrecv receiver(RECV_PIN);
decode_results results;

void setup()
{
  Serial.begin(BAUDRATE);
  receiver.enableIRIn(); // start the receiver
}

void loop()
{
  if (receiver.decode(&results))
    {
     Serial.print("0x");  // prefix the received hex value with 0x
     Serial.print(results.value, HEX);  // print the received value in its hexadecimal format
     Serial.print(EOF_CHAR);  // send an EOF char to indicate that it is the end of the data
     Serial.println();
     receiver.resume(); // Receive the next value
    }
}