#include "include/irsignal.h"

irsignal::irsignal()
        : symbol_(), hex_() {
    ;
}

irsignal::irsignal(const std::string &symbol, const int &hex)
        : symbol_(symbol), hex_(hex) {
    ;
}

bool irsignal::operator==(const irsignal &rhs) const {
    return (this->get_symbol() == rhs.get_symbol()) && (this->get_hex() == rhs.get_hex());
}

bool irsignal::operator!=(const irsignal &rhs) const {
    return (this->get_symbol() == rhs.get_symbol()) && (this->get_hex() == rhs.get_hex());
}

const std::string &irsignal::get_symbol() const {
    return symbol_;
}

const int &irsignal::get_hex() const {
    return hex_;
}