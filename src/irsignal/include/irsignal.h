#ifndef IR_SIGNAL_H
#define IR_SIGNAL_H

#include <string>
#include <map>

class irsignal {
public:
    irsignal();

    irsignal(const std::string &symbol, const int &hex);

    bool operator==(const irsignal &rhs) const;

    bool operator!=(const irsignal &rhs) const;

    const std::string &get_symbol() const;

    const int &get_hex() const;

private:
    std::string symbol_;
    int hex_;
};


#endif //IR_SIGNAL_H
