#include <iostream>
#include "irsignal.h"
#include <vector>
#include <unistd.h>
#include <fcntl.h>
#include <sstream>
#include <algorithm>
#include <cstring>

std::string read_serial(const char *port) {
    int device = open(port, O_RDONLY | O_NOCTTY, O_SYNC);
    int pos = 0;
    char buf;
    std::string response;
    char end = '!'; // use a specific character to signify the end of the data stream
    do {
        // n - number of bytes read
        ssize_t n = read(device, &buf, sizeof(buf));

        if (n > 0) {    // some bytes have been read
	    if (buf != end) {
                response += buf;
                pos += n;
            }
        } else if (n == 0) {
            std::cout << "No bytes read\n";
        } else {    // n is negative, error
            std::cerr << "Error reading serial: " << strerror(errno) << '\n';
            break;
        }

    } while (buf != end);

    return response;
}

int main() {
    // possible signals and their corresponding symbols for our remote
    const std::vector<irsignal> signals = {
	    irsignal("CH-", 0xFFA25D),
            irsignal("CH", 0xFF629D),
            irsignal("CH+", 0xFFE21D),
            irsignal("|<<", 0xFF22DD),
            irsignal(">>|", 0xFF02FD),
            irsignal("|>||", 0xFFC23D),
            irsignal("-", 0xFFE01F),
            irsignal("+", 0xFFA857),
            irsignal("EQ", 0xFF906F),
            irsignal("0", 0xFF6897),
            irsignal("100+", 0xFF9867),
            irsignal("200+", 0xFFB04F),
            irsignal("1", 0xFF30CF),
            irsignal("2", 0xFF18E7),
            irsignal("3", 0xFF7A85),
            irsignal("4", 0xFF10EF),
            irsignal("5", 0xFF38C7),
            irsignal("6", 0xFF5AA5),
            irsignal("7", 0xFF42BD),
            irsignal("8", 0xFF48B5),
            irsignal("9", 0xFF52AD)
    };

    const char *port = "/dev/ttyACM0";
    std::string response = read_serial(port);
    std::istringstream iss(response);
    std::string line;
    while (std::getline(iss, line)) {   // read the response line by line
        std::stringstream ss;
        // convert the hex number stored in a string into an int
        int serial_message;
        ss << std::hex << line;
        ss >> serial_message;
        irsignal received_signal;
        // is there a an object in signals with the same value as the one we read?
        for (const auto &signal : signals) {
            if (serial_message == signal.get_hex()) {
                irsignal new_signal(signal.get_symbol(), serial_message);
                received_signal = new_signal;
            }
        }
        const bool signal_exists = (std::find(signals.begin(), signals.end(), received_signal) != signals.end());
        if (signal_exists) {
            std::cout << received_signal.get_symbol() << '\n';
        } else {
            std::cerr << line << " does not exist\n";
        }
    }

    return 0;
}