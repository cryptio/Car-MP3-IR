   ------------------
 /                    \
|  CH-    CH     CH+   |
| FFA25D FF629D FFE21D |
|                      |
|  |<<     >>|   |>||  |
| FF22DD FF02FD FFC23D |
|                      |
|   -       +     EQ   |
| FFE01F FFA857 FF906F |
|                      |
|   0      100+  200+  |
| FF6897 FF9867 FFB04F |
|                      |
|   1       2     3    |
| FF30CF FF18E7 FF7A85 |
|                      |
|   4       5     6    |
| FF10EF FF38C7 FF5AA5 |
|                      |
|   7       8     9    |
| FF42BD FF4AB5 FF52AD |
|                      |
|         Car          |
|         mp3          |
 \                    /
   ------------------
FFFFFFFF – repeat, gets sent when a button is held

Range: +8 meters
Launch tube infrared wavelength: 940Nm
Crystal: oscillation frequency of 455 KHz
IR carrier frequency: 38KHz
Encoding: NEC encoding format, upd6122 encoding scheme, 00FF user code
Size: 86 * 40 * 6mm
Frequency: 38KHz
Power supply: CR2025/160mAH
Button: free height is less than 3mm, the force 200-350g, the life of more than 200,000